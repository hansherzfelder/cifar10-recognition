# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 15:25:49 2019

@author: Hans Herzfelder
"""

from Cifar10 import Cifar
import os
from ImageCifar import Recognition

def menu():
	"""
   Function that cleans the screen and shows the menu again
	"""
	os.system('clear') # NOTA para windows tienes que cambiar clear por cls
	print ("Option Menu")
	print ("\t1 - Create a model")
	print ("\t2 - Detect object in image")
	print ("\t9 - Exit")


def ProcessMenu():
    K = ["AirPlane","Car","Bird","Cat","Deer","Dog","Frog","Horse","Ship","Truck"]
    counter = 0
    for i in K:
        counter = counter + 1        
        print("\t",counter,"-",i)

K = ["AirPlane","Car","Bird","Cat","Deer","Dog","Frog","Horse","Ship","Truck"]

while True:
    
    menu()

    opt = int(input("select an option: "))
    
    if opt == 1:
        
        while True:
            print("")
            print("What model would you like to create?")
            print("")
        
            ProcessMenu()
        
            opt2 = int(input("select an option: "))
            opt2 = opt2 - 1
        
            if opt2 < 10:
                x = Cifar(opt2)
                x.process()
                input("finished model...\nPress enter to return to the menu")
                break
            else:
                print("The selected option is not valid")
            
    elif opt == 2:
        
        opt3 = input("Enter the name of the image: ")
        opt4 = opt3
        while os.path.isfile(opt4) != True:
            print("The file has not been found try with another name")
            opt3 = input("Enter the name of the image")
            opt4 = opt3
        else:
            while True:
                print("")
                print("What would you like to detect in your image?")
                print("")
            
                ProcessMenu()
                opt5 = int(input("select an option: "))
                opt5 = opt5 - 1
                opt6 = K[opt5]+"Detector.h5"
                
                if os.path.isfile(opt6):
                    y = Recognition(opt4,opt6)
                    y.detector()
                    input("finished search...\nPress enter to return to the menu")
                    break
                else:
                    print("The selected option is not valid")
    
    elif opt == 9:
        
        break
    
    else:
        print("")
        input("You have not pressed any correct option...\nPress enter to return to the menu")    
                    
                    
            

            
        
#x = cifar(1)
#x.process()