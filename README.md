#Cifar 10

Use of the data set cifar10 for the recognition of any of its ten classes in an image

##Installation and Execution Instructions

1. Install Python, Tensorflow, Keras, Numpy, PIL
2. Recommendation the use of a GPU to accelerate the program execution process
3. Search for Main.py
4. Follow the options menu inside main.py
5. Important you must create the model for each of the classes before looking for that class in an image