# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 14:55:58 2019

@author: Hans Herzfelder
"""

import tensorflow as tf
from tensorflow import keras

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
keras.backend.set_session(sess)

from keras.datasets import cifar10

(X_train,Y_train),(X_test, Y_test) = cifar10.load_data()

import numpy as np

X_train = X_train.astype(np.float32) / 255.
X_test = X_test.astype(np.float32) / 255.


from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import RMSprop



class Cifar():
      def __init__(self,ClassDetector,batch_size=128):
        self.ClassDetector = ClassDetector
        self.batch_size = batch_size
      
      def process(self):
        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3), input_shape=(32, 32, 3), activation="relu", padding="same"))
        model.add(Conv2D(32, kernel_size=(3, 3), activation="relu", padding="same"))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Conv2D(64, kernel_size=(3, 3), activation="relu", padding="same"))
        model.add(Conv2D(64, kernel_size=(3, 3), activation="relu", padding="same"))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Conv2D(128, kernel_size=(3, 3), activation="relu", padding="same"))
        model.add(Conv2D(128, kernel_size=(3, 3), activation="relu", padding="same"))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Flatten())
        model.add(Dense(256, activation="relu"))
        model.add(Dense(128, activation="relu"))
        model.add(Dense(1, activation="sigmoid"))

        model.compile(optimizer=RMSprop(lr=0.0001), loss="binary_crossentropy", metrics=["accuracy"])

        Y_class_train = Y_train == self.ClassDetector
        gen = ImageDataGenerator(width_shift_range=3, height_shift_range=3, zoom_range=0.1, horizontal_flip=True)
        model.fit_generator(gen.flow(X_train, Y_class_train, batch_size = self.batch_size, shuffle=True), epochs=1, workers=8,steps_per_epoch=len(X_train)/ self.batch_size)
          
        print(model.evaluate(X_train, Y_class_train))
          
        Y_class_test = Y_test == self.ClassDetector
        print(model.evaluate(X_test, Y_class_test))
          
          
        K = ["AirPlane","Car","Bird","Cat","Deer","Dog","Frog","Horse","Ship","Truck"]  
     
        model.save(K[self.ClassDetector]+"Detector.h5")
