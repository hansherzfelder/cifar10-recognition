# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 15:01:15 2019

@author: Hans Herzfelder
"""

import tensorflow as tf
from tensorflow import keras

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
keras.backend.set_session(sess)

from PIL import Image
import numpy as np
from PIL import ImageDraw

import keras.models as models

class Recognition():
      def __init__(self,image,filename):
        self.image = image
        self.filename = filename
        
      def detector(self):
        img = Image.open(self.image)
        img = img.resize((round(img.size[0] / 3), round(img.size[1] / 3)), resample=Image.BICUBIC) 
        
        model = models.load_model(self.filename)
         
        sizes = [200, 150, 100, 75]
        step_size = 10

        pictures = [] 

        for size in sizes:
            for x in range(0, img.size[0] - size, step_size):
                for y in range(0, img.size[1] - size, step_size):
                    part = img.crop((x, y, x + size, y + size))
                    data = np.asarray(part.resize((32, 32), resample=Image.BICUBIC))
                    data = data.astype(np.float32) / 255.

                    pred = model.predict(data.reshape(-1, 32, 32, 3))
                    if pred[0][0] > 0.98:
                        # print(pred[0][0])
                        pictures.append((x, y, size))

        out = img.copy()
        draw = ImageDraw.Draw(out)

        pictures_drawn = []

        for picture in pictures:
            exists = False
            for picture_drawn in pictures_drawn:
                if picture[0] >= picture_drawn[0] and picture[0] <= picture_drawn[0] + picture_drawn[2]:
                    if picture[1] >= picture_drawn[1] and picture[1] <= picture_drawn[1] + picture_drawn[2]:
                        exists = True
                
                
            if exists == False:
                points = [
                        (picture[0], picture[1]),
                        (picture[0], picture[1] + picture[2]),
                        (picture[0] + picture[2], picture[1] + picture[2]),
                        (picture[0] + picture[2], picture[1]),
                        (picture[0], picture[1])
                        ]   
                draw.line(points, "yellow", 5)
                pictures_drawn.append(picture)
                print(picture)
        
        out.show()